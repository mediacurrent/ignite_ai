<?php

namespace Drupal\ignite_ai\Commands;

use Drush\Commands\DrushCommands;
use GuzzleHttp\Client;
use Drupal\Core\Site\Settings;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\block_content\Entity\BlockContent;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\media\Entity\Media;
use GuzzleHttp\Exception\RequestException;

/**
 * A Drush commandfile.
 */
class IgniteAiCommands extends DrushCommands {

  /**
   * Command description here.
   *
   * @param $arg1
   *   First word of the AI prompt.
   * @param $arg2
   *   Second word of the AI prompt.
   * @param $arg3
   *   Third word of the AI prompt.
   * @param $arg4
   *   Fourth word of the AI prompt.
   * @param $arg5
   *   Fifth word of the AI prompt.
   * @usage ignite_ai-createHomepage ai-homepage
   *   Usage description
   *
   * @command ignite_ai:createHomepage
   * @aliases ai-homepage
   */
  public function createHomepage($arg1, $arg2 = '', $arg3 = '', $arg4 = '', $arg5 = '') {
    $args = trim($arg1 . ' ' . $arg2 . ' ' . $arg3 . ' ' . $arg4 . ' ' . $arg5);

    // Get menu links from AI.
    $links = trim(preg_replace('/\s+/', ' ', str_replace('"', '', self::getCompletion('5 main menu link titles for a website with topic: "' . $args . '", return in a pipe delimited.', 100))));
    $links = explode('|', $links);

    // Create menu links.
    if (is_array($links) && count($links) == 5) {
      self::createMenus($links);
    }

    // Create AI generated logo logo.
    $image_url = self::getImage('Stock logo for topic: ' . $args, '256x256');

    $data = file_get_contents($image_url);
    $file = \Drupal::service('file.repository')->writeData($data, 'public://logo.png');
    $file->setPermanent();
    $file->save();

    // Set theme logo to custom logo.
    $config = \Drupal::configFactory()->getEditable('ignite_theme.settings');
    $config->set('logo.path', $file->getFileUri());
    $config->set('logo.use_default', FALSE);

    // Change colors of theme.
    $hex_codes = trim(preg_replace('/\s+/', ' ', self::getCompletion('3 hex codes (primary, secondary, dark) for a website with topic: "' . $args . '", return in a pipe delimited list.', 50)));
    $hex_codes = explode('|', $hex_codes);

    if (is_array($hex_codes) && count($hex_codes) == 3) {
      $config->set('primary_color', trim($hex_codes[0]));
      $config->set('secondary_color', trim($hex_codes[1]));
      $config->set('dark_color', trim($hex_codes[2]));
    }

    $config->save();

    // Create homepage layout node.
    $node = Node::create([
      'type' => 'layout',
      'title' => 'Welcome!',
      'uid' => 1,
      'status' => 1,
      'promote' => 1,
      'sticky' => 1,
      'body' => [
        'value' => '',
        'format' => 'full_html',
      ],
      'field_hide_page_title' => TRUE,
      'path' => [
        'alias' => '/welcome',
        'pathauto' => 0,
      ],
    ]);

    $node->save();

    $section = new Section('layout_onecol');

    // List of block function names.
    $blocks = [];

    $blocks[] = self::createHero($args);
    $blocks[] = self::create5050($args);
    $blocks[] = self::create5050($args, 'left');
    $blocks[] = self::createBanner($args);
    $blocks[] = self::createAccordion($args);

    // Create block entities and add them to the layout.
    foreach ($blocks as $block) {
      $block->save();

      $component = new SectionComponent(
        $block->id(), 'content', [
          'id' => 'block_content:' . $block->uuid(),
          'label' => $block->label(),
          'provider' => 'block_content',
          'label_display' => '0',
        ]
      );
      $section->appendComponent($component);
    }

    // Add the section to the layout.
    $node->get('layout_builder__layout')->appendItem($section);
    $node->save();

    $url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();

    $this->logger()->success(dt('Homepage created at @url', ['@url' => $url]));
  }

  /**
   * Create a Hero block.
   *
   * @param string $args
   *   Arguments for the AI prompt.
   *
   * @return \Drupal\block_content\Entity\BlockContent
   *   The Hero block.
   */
  protected static function createHero($args) {
    $image_url = self::getImage('Stock hero image for topic: ' . $args . ' on white background');

    // Create hero image.
    $data = file_get_contents($image_url);
    $file = \Drupal::service('file.repository')->writeData($data, 'public://homepage-hero.png');
    $media = Media::create([
      'bundle' => 'image',
      'name' => 'Hero Image',
      'field_image' => [
        'target_id' => $file->id(),
        'alt' => 'Hero Image',
        'title' => 'Hero Image',
      ],
    ]);

    $media->save();

    // Remove quotes from the heading.
    $heading = trim(preg_replace('/\s+/', ' ', str_replace('"', '', self::getCompletion('Tagline for topic: ' . $args, 30))));
    $eyebrow = trim(preg_replace('/\s+/', ' ', str_replace('"', '', self::getCompletion('Short eyebrow label for topic: ' . $args, 10))));
    $summary = trim(preg_replace('/\s+/', ' ', self::getCompletion('Hero summary for topic: ' . $args . ', maximum 100 words.', 100)));

    $block = BlockContent::create([
      'type' => 'hero',
      'info' => 'Hero: Homepage',
      'field_eyebrow' => $eyebrow,
      'field_link' => [
        'uri' => 'https://www.drupal.org',
        'title' => 'Read more',
      ],
      'field_media' => [
        'target_id' => $media->id(),
      ],
      'field_heading' => [
        'value' => $heading,
        'format' => 'basic_html',
      ],
      'field_summary' => [
        'value' => $summary,
        'format' => 'basic_html',
      ],
      'field_hero_layout' => 'image_left',
    ]);

    return $block;
  }

  /**
   * Get OpenAI completion.
   *
   * @param string $prompt
   *   The AI prompt.
   * @param int $max_tokens
   *   The maximum number of tokens to return.
   *
   * @return string
   *   The AI completion response.
   */
  public static function getCompletion($prompt, $max_tokens) {
    $api_key = Settings::get('api_key');
    $url = 'https://api.openai.com/v1/completions';
    $temperature = 0.7;
    $model = 'text-davinci-003';

    $client = new Client();

    try {
      $response = $client->request('POST', $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $api_key,
          'Content-Type' => 'application/json',
        ],
        'json' => [
          'prompt' => $prompt,
          'temperature' => $temperature,
          'max_tokens' => $max_tokens,
          'model' => $model,
        ],
      ]);
    }
    catch (RequestException $e) {
      \Drupal::logger('ignite_ai')->error($e->getMessage());
      return FALSE;
    }

    if ($response->getStatusCode() != 200) {
      \Drupal::logger('ignite_ai')->error($response->getBody()->getContents());
      return FALSE;
    }
    else {
      return json_decode($response->getBody()->getContents())->choices[0]->text;
    }
  }

  /**
   * Get OpenAI image response.
   *
   * @param string $prompt
   *   The AI prompt.
   * @param string $size
   *   The image size.
   *
   * @return string
   *   The AI image response.
   */
  public static function getImage($prompt, $size = '512x512') {
    $api_key = Settings::get('api_key');
    $url = 'https://api.openai.com/v1/images/generations';

    $client = new Client();

    try {
      $response = $client->request('POST', $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $api_key,
          'Content-Type' => 'application/json',
        ],
        'json' => [
          'prompt' => $prompt,
          'n' => 1,
          'size' => $size,
        ],
      ]);
    }
    catch (RequestException $e) {
      \Drupal::logger('ignite_ai')->error($e->getMessage());
    }

    if ($response->getStatusCode() != 200) {
      \Drupal::logger('ignite_ai')->error($response->getBody()->getContents());
      return FALSE;
    }
    else {
      return json_decode($response->getBody()->getContents())->data[0]->url;
    }
  }

  /**
   * Create menu items.
   *
   * @param array $links
   *   An array of menu links.
   */
  protected static function createMenus(array $links) {
    // Delete all main menu links.
    $menu_links = \Drupal::entityTypeManager()
      ->getStorage('menu_link_content')
      ->loadByProperties(['menu_name' => 'main']);
    foreach ($menu_links as $menu_link) {
      $menu_link->delete();
    }

    $weight = 0;

    // Create menu items.
    foreach ($links as $link_name) {
      $menu_link = MenuLinkContent::create([
        'title' => $link_name,
        'link' => ['uri' => 'internal:/welcome'],
        'menu_name' => 'main',
        'expanded' => TRUE,
        'weight' => $weight++,
      ]);
      $menu_link->save();
    }
  }

  /**
   * Create a Accordion block.
   *
   * @param string $args
   *   Arguments for the AI prompt.
   *
   * @return \Drupal\block_content\Entity\BlockContent
   *   The Accordion block.
   */
  protected static function createAccordion($args) {
    // Create Accordion block.
    $block = BlockContent::create([
      'type' => 'accordion',
      'field_title' => 'Frequently Asked Questions',
    ]);

    $block->save();

    // Get menu links from AI.
    $faq_titles = trim(preg_replace('/\s+/', ' ', self::getCompletion('7 sample FAQ headings for a website with topic: "' . $args . '", return in a pipe delimited and a space after each number.', 200)));
    $faq_titles = explode('|', $faq_titles);

    foreach ($faq_titles as $title) {
      if (!empty($title)) {
        $paragraph = Paragraph::create([
          'type' => 'accordion_item',
          'field_title' => trim(preg_replace('/\s+/', ' ', $title)),
          'field_body' => [
            'format' => 'full_html',
            'summary' => '',
            'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus auctor, turpis at luctus finibus, erat lectus convallis velit, at sodales purus lacus quis magna. Curabitur imperdiet sapien libero, fringilla ullamcorper nibh ullamcorper vitae. Proin sed luctus augue. Nam fringilla enim leo, non sodales ante gravida ac.</p>',
          ],
        ]);

        $paragraph->save();

        $block->get('field_accordion_item')->appendItem($paragraph);
      }
    }

    return $block;
  }

  /**
   * Create a 5050 block.
   *
   * @param string $args
   *   Arguments for the AI prompt.
   * @param string $layout
   *   The layout of the block.
   *
   * @return \Drupal\block_content\Entity\BlockContent
   *   The 50/50 block.
   */
  protected static function create5050($args, $layout = 'right') {
    // Create 50/50 block.
    $image_url = self::getImage('Teaser thumbnail for topic: ' . $args . ' on white background');

    $data = file_get_contents($image_url);
    $file = \Drupal::service('file.repository')->writeData($data, 'public://teaser.jpg');
    $media = Media::create([
      'bundle' => 'image',
      'name' => 'Teaser Image',
      'field_image' => [
        'target_id' => $file->id(),
        'alt' => 'Teaser Image',
        'title' => 'Teaser Image',
      ],
    ]);

    $media->save();

    $heading = trim(preg_replace('/\s+/', ' ', str_replace('"', '', self::getCompletion('Tagline for topic: ' . $args, 30))));
    $eyebrow = trim(preg_replace('/\s+/', ' ',  str_replace('"', '', self::getCompletion('Short eyebrow label for topic: ' . $args, 10))));
    $summary = trim(preg_replace('/\s+/', ' ', self::getCompletion('Hero summary for topic: ' . $args . ', maximum 100 words.', 100)));

    $block = BlockContent::create([
      'type' => 'teaser',
      'field_title' => $heading,
      'field_eyebrow' => $eyebrow,
      'field_link' => [
        'uri' => 'https://www.drupal.org',
        'title' => 'Learn more',
      ],
      'field_media' => [
        'target_id' => $media->id(),
      ],
      'field_summary' => $summary,
      'field_teaser_layout' => $layout,
    ]);

    return $block;
  }

  /**
   * Create a Banner block.
   *
   * @param string $args
   *   The AI prompt.
   *
   * @return \Drupal\block_content\Entity\BlockContent
   *   The Banner block.
   */
  protected static function createBanner($args) {
    $heading = trim(preg_replace('/\s+/', ' ', str_replace('"', '', self::getCompletion('Tagline for topic: ' . $args, 30))));
    $eyebrow = trim(preg_replace('/\s+/', ' ', str_replace('"', '', self::getCompletion('Short eyebrow label for topic: ' . $args, 10))));
    $summary = trim(preg_replace('/\s+/', ' ', self::getCompletion('Hero summary for topic: ' . $args . ', maximum 100 words.', 100)));

    // Create Banner block.
    $block = BlockContent::create([
      'type' => 'banner',
      'field_title' => $heading,
      'field_summary' => $summary,
      'field_eyebrow' => $eyebrow,
      'field_link' => [
        'uri' => 'https://www.drupal.org',
        'title' => 'Explore features',
      ],
    ]);

    return $block;
  }

}
